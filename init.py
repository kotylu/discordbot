import os


class Initialiser():
    def __init__(self, client):
        self.client = client

    def run(self):
        self.init_greetings()
        self.init_channels()

    def init_greetings(self):
        self.client.add_greeting("How do you do?")
        self.client.add_greeting("Pleased to meet you")
        self.client.add_greeting("Nice to meet you")
        self.client.add_greeting("How have you been?")
        self.client.add_greeting("Ey")
        self.client.add_greeting("Hey man, how are ya")
        self.client.add_greeting("Long-time no see huh?")
        self.client.add_greeting("Yo")
        self.client.add_greeting("What's up, man")
        self.client.add_greeting("Ey mate")

    def init_channels(self):
        self.client.add_channel("test", os.getenv("TEST_CHAN_ID"))
        self.client.add_channel("smoke", os.getenv("SMOKE_CHAN_ID"))
