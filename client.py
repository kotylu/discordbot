import discord, random
from meta import Data
from smoke import SmokeBreak


class Bot(discord.Client):
    def __init__(self):
        discord.Client.__init__(self)
        self.channels = dict()
        self.smoker = SmokeBreak()
        self.greetings = list()
        self.commands = {
                    "greet": self.greet,
                    "cigo": self.cigo,
                    "c": self.cigo
                }

    def add_channel(self, name, channel_id):
        self.channels[name] = channel_id

    def add_greeting(self, greeting):
        self.greetings.append(greeting)

    async def on_ready(self):
        print("logged in")
        for key, value in self.channels.items():
            self.channels[key] = self.get_channel(int(value))

    async def on_message(self, message):
        if message.author == self.user:
            return
        data = Data(message)
        if data.channel in self.channels.values():
            if message.content[0] is "!":
                split = message.content.split(" ")
                cmd, args = split[0][1:], split[1:]
                if cmd in self.commands.keys():
                    await self.commands[cmd](data, args)

    def get_greeting(self):
        randix = random.randint(0, len(self.greetings)-1)
        return self.greetings[randix]

    async def greet(self, data, _):
        greeting = self.get_greeting()
        await data.channel.send("{0} {1}".format(data.author.mention, greeting))

    async def cigo(self, data, args):
        #TODO checker if member already going smoke
        #TODO specify channel
        #if "specify:<channel>" in args set smoker.channel = <channel>
        if "?" in args:
            self.smoker.set_channel(self.channels["smoke"])
            self.smoker.add_member(data.author)
            await data.channel.send("dame cigo?")
        if "yes" in args:
            self.smoker.add_member(data.author)
            str_members = ", ".join(member.mention for member in self.smoker.members)
            await data.channel.send("{0} pujde na cigo s {1}".format(data.author.mention, str_members))
        if "letsgo" in args:
            str_members = ", ".join(member.mention for member in self.smoker.members)
            await data.channel.send("tak jdem debilci {0}".format(str_members))
            await self.smoker.start()
