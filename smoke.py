class SmokeBreak():
    def __init__(self):
        self.members = list()
        self.channel = int()

    def set_channel(self, value):
        self.channel = value

    def add_member(self, member):
        if member not in self.members:
            self.members.append(member)
            return True
        return False

    async def start(self):
        for member in self.members:
            await member.move_to(self.channel)
        self.members = list()

