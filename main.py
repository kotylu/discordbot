import discord, os
from dotenv import load_dotenv
from client import Bot
from init import Initialiser

load_dotenv()
bot = Bot()
initer = Initialiser(bot)

initer.run()
bot.run(os.getenv("TOKEN"))
